var OregonH = OregonH || {};
 
OregonH.Caravan = {};
 
OregonH.Caravan.init = function(stats){
  this.day = stats.day;
  this.distance = stats.distance;
  this.crew = stats.crew;
  this.food = stats.food;
  this.oxen = stats.oxen;
  this.firepower = stats.firepower;
  this.money = stats.money;
};

OregonH.Caravan.updateWeight = function(){
    var droppedFood = 0;
    var droppedGuns = 0;
}  